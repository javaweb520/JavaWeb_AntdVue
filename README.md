## 📚 项目介绍
一款 Java 语言基于 SpringBoot2.x、MybatisPlus、Vue、AntDesign、MySQL等框架精心打造的一款前后端分离框架，致力于实现模块化、组件化、可插拔的前后端分离架构敏捷开发框架，可用于快速搭建前后端分离后台管理系统，本着简化开发、提升开发效率的初衷，目前框架已集成了完整的RBAC权限架构和常规基础模块，前端Vue端支持多主题切换，可以根据自己喜欢的风格选择想一个的主题，实现了个性化呈现的需求；

为了敏捷快速开发，提升研发效率，框架内置了一键CRUD代码生成器，自定义了模块生成模板，可以根据已建好的表结构(字段注释需规范)快速的一键生成整个模块的所有代码和增删改查等等功能业务，真正实现了低代码开发，极大的节省了人力成本的同时提高了开发效率，缩短了研发周期，是一款真正意义上实现组件化、低代码敏捷开发框架。

## 🍪 内置模块
+ 用户管理：用于维护管理系统的用户，常规信息的维护与账号设置。
+ 角色管理：角色菜单管理与权限分配、设置角色所拥有的菜单权限。
+ 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
+ 职级管理：主要管理用户担任的职级。
+ 岗位管理：主要管理用户担任的岗位。
+ 部门管理：主要管理系统组织架构，对组织架构进行统一管理维护。
+ 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
+ 登录日志：系统登录日志记录查询包含登录异常。
+ 字典管理：对系统中常用的较为固定的数据进行统一维护。
+ 配置管理：对系统的常规配置信息进行维护，网站配置管理功能进行统一维护。
+ 城市管理：统一对全国行政区划进行维护，对其他模块提供行政区划数据支撑。
+ 友链管理：对系统友情链接、合作伙伴等相关外链进行集成维护管理的模块。
+ 个人中心：主要是对当前登录用户的个人信息进行便捷修改的功能。
+ 广告管理：主要对各终端的广告数据进行管理维护。
+ 站点栏目：主要对大型系统网站等栏目进行划分和维护的模块。
+ 会员管理：对各终端注册的会员进行统一的查询与管理的模块。
+ 网站配置：对配置管理模块的数据源动态解析与统一维护管理的模块。
+ 通知公告：系统通知公告信息发布维护。
+ 代码生成：一键生成模块CRUD的功能，包括后端和前端等相关代码。
+ 案例演示：常规代码生成器一键生成后的演示案例。

## 👷 软件信息
+ 软件名称：JavaWeb敏捷开发框架单体AntdVue版本
+ 软件作者：@鲲鹏
+ 软件出处：上海JavaWeb研发中心
+ 软件协议：Apache-2.0
+ 官网网址：[https://www.javaweb.vip](https://www.javaweb.vip)  
+ 文档网址：[http://docs.antdvue.javaweb.vip](http://docs.antdvue.javaweb.vip)  

## 🎨 系统演示

演示地址：http://manage.antdvue.javaweb.vip

账号 | 密码| 操作权限
---|---|---
admin | 123456| 演示环境无法进行修改删除操作

## 📌 版本说明

|  版本名称 |   说明 | 地址
|:------:|:-------:| :------:
| JavaWeb_Layui 单体混编专业版 | 采用SpringBoot2、Thymeleaf、layui、MySQL | https://gitee.com/javaweb520/JavaWeb_Layui
| avaWeb_Layui 单体混编旗舰版 | 采用SpringBoot2、Thymeleaf、layui、MySQL | https://gitee.com/javaweb520/JavaWeb_Layui_Pro
| JavaWeb_EleVue单体前后端分离版 | 采用SpringBoot2、Vue2.x、ElementUI、MySQL | https://gitee.com/javaweb520/JavaWeb_EleVue
| JavaWeb_AntdVue单体前后端分离版 | 采用SpringBoot2、Vue3.x、AntDesign、MySQL | https://gitee.com/javaweb520/JavaWeb_AntdVue
| JavaWeb_Cloud_Eureka_EleVue微服务前后端分离版 | 采用SpringCloud、Eureka、OAuth2、Vue2.x、ElementUI、MySQL | https://gitee.com/javaweb520/JavaWeb_Cloud_Eureka_EleVue
| JavaWeb_Cloud_Eureka_AntdVue微服务前后端分离版 | 采用SpringCloud、Eureka、OAuth2、Vue3.x、AntDesign、MySQL | https://gitee.com/javaweb520/JavaWeb_Cloud_Eureka_AntdVue
| JavaWeb_Cloud_Consul_EleVue微服务前后端分离版 | 采用SpringCloud、Consul、OAuth2、Vue2.x、ElementUI、MySQL | https://gitee.com/javaweb520/JavaWeb_Cloud_Consul_EleVue
| JavaWeb_Cloud_Consul_AntdVue微服务前后端分离版 | 采用SpringCloud、Consul、OAuth2、Vue3.x、AntDesign、MySQL | https://gitee.com/javaweb520/JavaWeb_Cloud_Consul_AntdVue
| JavaWeb_Cloud_Nacos_EleVue微服务前后端分离版 | 采用SpringCloud、Nacos、OAuth2、Vue2.x、ElementUI、MySQL | https://gitee.com/javaweb520/JavaWeb_Cloud_Nacos_EleVue
| JavaWeb_Cloud_Nacos_AntdVue微服务前后端分离版 | 采用SpringCloud、Nacos、OAuth2、Vue3.x、AntDesign、MySQL | https://gitee.com/javaweb520/JavaWeb_Cloud_Nacos_AntdVue

## 🔧 模块展示

![效果图1](./uploads/demo/1.png)

![效果图2](./uploads/demo/2.png)

![效果图3](./uploads/demo/3.png)

![效果图4](./uploads/demo/4.png)

![效果图5](./uploads/demo/5.png)

![效果图5](./uploads/demo/6.png)

![效果图7](./uploads/demo/7.png)

![效果图8](./uploads/demo/8.png)

![效果图9](./uploads/demo/9.png)

![效果图10](./uploads/demo/10.png)

![效果图11](./uploads/demo/11.png)

![效果图12](./uploads/demo/12.png)

![效果图13](./uploads/demo/13.png)

![效果图14](./uploads/demo/14.png)

![效果图15](./uploads/demo/15.png)

![效果图16](./uploads/demo/16.png)

![效果图17](./uploads/demo/17.png)

![效果图18](./uploads/demo/18.png)

![效果图19](./uploads/demo/19.png)

![效果图20](./uploads/demo/20.png)

![效果图21](./uploads/demo/21.png)

![效果图22](./uploads/demo/22.png)

![效果图23](./uploads/demo/23.png)

![效果图24](./uploads/demo/24.png)

![效果图25](./uploads/demo/25.png)

![效果图26](./uploads/demo/26.png)

## ✨  特别鸣谢
感谢[MybatisPlus](https://mp.baomidou.com/)、[VueJs](https://cn.vuejs.org/)、[AntDesign](https://2x.antdv.com/components/overview-cn/)等优秀开源项目。

## 📚 版权信息

软件版权和最终解释权归JavaWeb研发团队所有，商业版使用需授权，未授权禁止恶意传播和用于商业用途，否则将追究相关人的法律责任。

本软件框架禁止任何单位和个人用于任何违法、侵害他人合法利益等恶意的行为，禁止用于任何违反我国法律法规的一切平台研发，任何单位和个人使用本软件框架用于产品研发而产生的任何意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、附带或衍生的损失等)，本团队不承担任何法律责任。本软件框架只能用于公司和个人内部的法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有Copyright © 2018~2024 [javaweb.vip](https://www.javaweb.vip) All rights reserved。

更多细节参阅 [LICENSE](LICENSE)