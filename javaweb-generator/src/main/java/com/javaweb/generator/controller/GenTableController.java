// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.generator.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.javaweb.common.config.CommonConfig;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.common.utils.StringUtils;
import com.javaweb.generator.dto.GenTableDto;
import com.javaweb.generator.entity.GenTable;
import com.javaweb.generator.entity.GenTableColumn;
import com.javaweb.generator.query.GenTableQuery;
import com.javaweb.generator.service.IGenTableColumnService;
import com.javaweb.generator.service.IGenTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 代码生成业务表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @date 2020-11-06
 */
@RestController
@RequestMapping("/generate")
public class GenTableController {

    @Autowired
    private IGenTableService genTableService;

    @Autowired
    private IGenTableColumnService genTableColumnService;

    /**
     * 获取业务表列表
     *
     * @param genTableQuery 查询条件
     * @return
     */
    @GetMapping("/index")
    public JsonResult index(GenTableQuery genTableQuery) {
        return genTableService.getList(genTableQuery);
    }

    /**
     * 获取数据库表
     *
     * @param query 查询条件
     * @return
     */
    @GetMapping("/genDbTableList")
    public JsonResult genDbTableList(GenTableQuery query) {
        IPage<GenTable> pageData = genTableService.genDbTableList(query);
        return JsonResult.success(pageData, "操作成功");
    }

    /**
     * 导入表
     *
     * @param tableNames 数据表
     * @return
     */
    @PostMapping("/importTable")
    public JsonResult importTable(@RequestBody String[] tableNames) {
        // 查询表信息
        List<GenTable> tableList = genTableService.selectDbTableListByNames(tableNames);
        genTableService.importGenTable(tableList);
        return JsonResult.success();
    }

    /**
     * 获取表详情信息
     *
     * @param tableId 表ID
     * @return
     */
    @GetMapping("/getTableInfo/{tableId}")
    public JsonResult getTableInfo(@PathVariable("tableId") String tableId) {
        GenTable table = genTableService.selectGenTableById(Integer.valueOf(tableId));
        List<GenTableColumn> list = genTableColumnService.selectGenTableColumnListByTableId(Integer.valueOf(tableId));
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("info", table);
        map.put("records", list);
        return JsonResult.success(map, "操作成功");
    }

    /**
     * 更新代码生成表信息
     *
     * @param genTable 生成表
     * @return
     */
    @PutMapping("/updateGenTable")
    public JsonResult updateGenTable(@Validated @RequestBody GenTable genTable) {
        if (CommonConfig.appDebug) {
            return JsonResult.error("演示环境禁止操作");
        }
        genTableService.validateEdit(genTable);
        genTableService.updateGenTable(genTable);
        return JsonResult.success();
    }

    /**
     * 删除业务表
     *
     * @param tableIds 业务表ID
     * @return
     */
    @DeleteMapping("/delete/{tableIds}")
    public JsonResult delete(@PathVariable("tableIds") Integer[] tableIds) {
        if (CommonConfig.appDebug) {
            return JsonResult.error("演示环境禁止操作");
        }
        return genTableService.delete(tableIds);
    }

    /**
     * 生成代码
     *
     * @param genTableDto 一键生成参数
     * @throws Exception
     */
    @PostMapping("/batchGenCode")
    public JsonResult batchGenCode(@RequestBody GenTableDto genTableDto) {
        if (CommonConfig.appDebug) {
            return JsonResult.error("演示环境禁止操作");
        }
        String[] tableNames = genTableDto.getTableNames().split(",");
        return genTableService.generatorCode(tableNames);
    }

    /**
     * 批量生成
     *
     * @param genTableDto 参数
     * @return
     */
    @PostMapping("/batchGenerate")
    public JsonResult batchGenerate(@RequestBody GenTableDto genTableDto) {
        if (CommonConfig.appDebug) {
            return JsonResult.error("演示环境禁止操作");
        }
        String tableName = genTableDto.getTableNames();
        if (StringUtils.isEmpty(tableName)) {
            return JsonResult.error("请选择数据表");
        }
        String[] tableNames = tableName.split(",");
        return genTableService.generatorCode(tableNames);
    }

}
