// +----------------------------------------------------------------------
// | JavaWeb敏捷开发框架 [ 赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | 版权所有 2018~2024 上海JavaWeb研发中心
// +----------------------------------------------------------------------
// | Licensed Apache-2.0 JavaWeb并不是自由软件，未经许可禁止去掉相关版权
// +----------------------------------------------------------------------
// | 官方网站: https://www.javaweb.vip
// +----------------------------------------------------------------------
// | Author: @鲲鹏 团队荣誉出品
// +----------------------------------------------------------------------
// | 版权和免责声明:
// | 本团队对该软件框架产品拥有知识产权（包括但不限于商标权、专利权、著作权、商业秘密等）
// | 均受到相关法律法规的保护，任何个人、组织和单位不得在未经本团队书面授权的情况下对所授权
// | 软件框架产品本身申请相关的知识产权，禁止用于任何违法、侵害他人合法权益等恶意的行为，禁
// | 止用于任何违反我国法律法规的一切项目研发，任何个人、组织和单位用于项目研发而产生的任何
// | 意外、疏忽、合约毁坏、诽谤、版权或知识产权侵犯及其造成的损失 (包括但不限于直接、间接、
// | 附带或衍生的损失等)，本团队不承担任何法律责任，本软件框架禁止任何单位和个人、组织用于
// | 任何违法、侵害他人合法利益等恶意的行为，如有发现违规、违法的犯罪行为，本团队将无条件配
// | 合公安机关调查取证同时保留一切以法律手段起诉的权利，本软件框架只能用于公司和个人内部的
// | 法律所允许的合法合规的软件产品研发，详细声明内容请阅读《框架免责声明》附件；
// +----------------------------------------------------------------------

package com.javaweb.system.controller;


import com.javaweb.common.annotation.Log;
import com.javaweb.common.common.BaseController;
import com.javaweb.common.enums.LogType;
import com.javaweb.common.utils.JsonResult;
import com.javaweb.system.entity.Dept;
import com.javaweb.system.query.DeptQuery;
import com.javaweb.system.service.IDeptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 部门表 前端控制器
 * </p>
 *
 * @author 鲲鹏
 * @date 2020-11-03
 */
@RestController
@RequestMapping("/dept")
public class DeptController extends BaseController {

    @Autowired
    private IDeptService deptService;

    /**
     * 获取部门列表
     *
     * @param deptQuery 查询条件
     * @return
     */
    @RequiresPermissions("sys:dept:index")
    @GetMapping("/index")
    public JsonResult index(DeptQuery deptQuery) {
        return deptService.getList(deptQuery);
    }

    /**
     * 添加部门
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "部门管理", logType = LogType.INSERT)
    @RequiresPermissions("sys:dept:add")
    @PostMapping("/add")
    public JsonResult add(@RequestBody Dept entity) {
        return deptService.edit(entity);
    }

    /**
     * 编辑部门
     *
     * @param entity 实体对象
     * @return
     */
    @Log(title = "部门管理", logType = LogType.UPDATE)
    @RequiresPermissions("sys:dept:edit")
    @PutMapping("/edit")
    public JsonResult edit(@RequestBody Dept entity) {
        return deptService.edit(entity);
    }

    /**
     * 删除部门
     *
     * @param deptId 部门ID
     * @return
     */
    @Log(title = "部门管理", logType = LogType.DELETE)
    @RequiresPermissions("sys:dept:delete")
    @DeleteMapping("/delete/{deptId}")
    public JsonResult delete(@PathVariable("deptId") Integer deptId) {
        return deptService.deleteById(deptId);
    }

    /**
     * 获取部门列表
     *
     * @return
     */
    @GetMapping("/getDeptList")
    public JsonResult getDeptList() {
        return deptService.getDeptList();
    }

}
